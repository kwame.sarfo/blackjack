package target.BlackJack.Modules;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import target.BlackJack.Enums.Strategy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PlayersTest {


    @Test void testGameDoesntStartWithOnePlayer()
    {
        Players player1 = new Players("Player1");


        List<Players> players = new ArrayList<>(Arrays.asList(player1));

        Game newGame = new Game(players, 5);

        Throwable ex = Assertions.assertThrows(OnePlayerCantPlayException.class, () -> newGame.takeTurn());

        Assertions.assertTrue(ex.getMessage().contains("You can't start this game with only One player"));

    }

    @Test
    public void eachPlayerGetsTwoCards()
    {

        Players player1 = new Players("Player1");
        Players player2 = new Players("Player2");
        Players player3 = new Players("Player3");

        List<Players> players = new ArrayList<>(Arrays.asList(player1, player2, player3));

        Game newGame = new Game(players, 5);

        Assertions.assertEquals(2, player1.getCurrentCards().size());
        Assertions.assertEquals(2, player2.getCurrentCards().size());
        Assertions.assertEquals(2, player3.getCurrentCards().size());
        Assertions.assertEquals(46, newGame.getCurrentDeck().size());

    }

    @Test
    public void playerWith15PointsHasHITStrategy()
    {
        Players player1 = new Players("Player1");
        Players player2 = new Players("Player2");
        Players player3 = new Players("Player3");
        Players player4 = new Players("Player4");
        Players player5 = new Players("Player5");
        Players player6 = new Players("Player6");

        List<Players> players = new ArrayList<>(Arrays.asList(player1, player2, player3, player4, player5, player6));

        Game newGame = new Game(players, 5);

        player1.setCurrentPoints(16);
        assertEquals(Strategy.HIT, player1.getCurrentStrategy());


    }


    @Test
    public void playerWith19PointsHasSTICKstrategy()
    {
        Players player1 = new Players("Player1");
        Players player2 = new Players("Player2");
        Players player3 = new Players("Player3");
        Players player4 = new Players("Player4");
        Players player5 = new Players("Player5");
        Players player6 = new Players("Player6");

        List<Players> players = new ArrayList<>(Arrays.asList(player1, player2, player3, player4, player5, player6));

        Game newGame = new Game(players, 5);

        player1.setCurrentPoints(19);
        assertEquals(Strategy.STICK, player1.getCurrentStrategy());


    }

    @Test
    public void playerWith22PointsHasGOBUSTstrategy()
    {
        Players player1 = new Players("Player1");
        Players player2 = new Players("Player2");
        Players player3 = new Players("Player3");
        Players player4 = new Players("Player4");
        Players player5 = new Players("Player5");
        Players player6 = new Players("Player6");

        List<Players> players = new ArrayList<>(Arrays.asList(player1, player2, player3, player4, player5, player6));

        Game newGame = new Game(players, 5);

        player1.setCurrentPoints(23);
        assertEquals(Strategy.GOBUST, player1.getCurrentStrategy());


    }

}