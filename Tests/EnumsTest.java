import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import target.BlackJack.Modules.Card;
import target.BlackJack.Modules.Deck;
import target.BlackJack.Enums.*;

import java.util.Collections;
import java.util.List;

public class EnumsTest {

    @Test
    public void getEnumValue()
    {
        NumberOnCard twoEnum = NumberOnCard.TWO;

        int twoEnumValue = twoEnum.getPoints();

        Assertions.assertEquals(2, twoEnumValue);
    }


    @Test
    public void populateDeck()
    {
        List<Card> deck = Deck.generateDeck();
        Collections.shuffle(deck);
        System.out.println(deck.toString());

        Assertions.assertEquals(52, deck.size());
    }
}
