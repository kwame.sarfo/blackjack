package target.BlackJack.Processes;

import target.BlackJack.Modules.Game;
import target.BlackJack.Modules.Players;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Players player1 = new Players("Player1");
        Players player2 = new Players("Player2");
        Players player3 = new Players("Player3");
        Players player4 = new Players("Player4");
        Players player5 = new Players("Player5");
        Players player6 = new Players("Player6");

        List<Players> players = new ArrayList<>(Arrays.asList(player1, player2, player3, player4, player5, player6));

        Game newGame = new Game(players, 5);

        newGame.play();
    }
}
