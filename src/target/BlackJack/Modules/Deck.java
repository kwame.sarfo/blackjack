package target.BlackJack.Modules;

import target.BlackJack.Enums.NumberOnCard;
import target.BlackJack.Enums.Suit;

import java.util.ArrayList;
import java.util.List;

public class Deck {

    public static List<Card> generateDeck()
    {
        List<Card> deck = new ArrayList<>();
        for(Suit suitName : Suit.values())
        {
            for(NumberOnCard numberOnCard : NumberOnCard.values())
            {
                deck.add(new Card(numberOnCard, suitName));
            }
        }
        return deck;
    }


}
