package target.BlackJack.Modules;

import target.BlackJack.Enums.Strategy;

import java.util.Collections;
import java.util.List;

public class Game {
    private List<Card> currentDeck;
    private List<Players> currentPlayers;
    private int currentRound;
    private int cutOffPoint;
    private boolean gameFinishStatus;


    private int maxNumberOfRounds;

    public Game(List<Players> players, int maxNumberOfRounds) {
        this.gameFinishStatus = false;
        this.setMaxNumberOfRounds(maxNumberOfRounds);
        this.generateCurrentDeck();
        this.cutOffPoint = 21;
        Collections.shuffle(currentDeck);
        this.currentPlayers = players;
        this.dealCards();
        this.currentRound = 1;
    }
    public void setMaxNumberOfRounds(int maxNumberOfRounds) {
        this.maxNumberOfRounds = maxNumberOfRounds;
    }


    public void play()
    {

        while(!this.gameFinishStatus)
        {
            System.out.println("ROUND " + this.currentRound);
            try{
                this.takeTurn();
            }catch (OnePlayerCantPlayException ex)
            {
                return;
            }

        }
        this.determinePlayerWithMaxPointsAtEndOfGame();
    }

    public void generateCurrentDeck(){
        this.currentDeck = Deck.generateDeck();

    }

    public List<Card> getCurrentDeck() {
        return currentDeck;
    }

    public void dealCards()
    {

        for(Players player : this.currentPlayers)
        {
            int lastElement = currentDeck.size() - 1;
            for(int iter = lastElement; iter >= lastElement-1; iter--)
            {

                player.receiveCard(currentDeck.get(iter));
                currentDeck.remove(iter);
            }
            player.setCurrentStrategy();
        }

    }



    public void dealAnotherCard(Players player)
    {
        int lastCardIndex = this.currentDeck.size() -1;
        Card lastCard = this.currentDeck.get(lastCardIndex);
        player.receiveCard(lastCard);
        System.out.println("Another card " + lastCard.getCardNumber() + " OF " + lastCard.getSuitName() +  " dealt to " + player.getPlayerName());
        System.out.println("TOTAL POINTS OF " + player.getPlayerName() + " NOW = " + player.getPointsOfCurrentCards());
        System.out.println(player.getPlayerName() + " strategy now = " + player.getCurrentStrategy());
        System.out.println('\n');
        this.currentDeck.remove(lastCardIndex);
        player.setCurrentStrategy();
    }

    public void ejectPlayer(Players player)
    {
        System.out.println("EJECTED " + player.getPlayerName());
        this.currentPlayers.remove(player);
    }

    public boolean checkIfAllPlayersStick()
    {
        boolean allPlayersStickStatus = true;
        for(int iter =0; iter < this.currentPlayers.size(); iter++)
        {
            if(this.currentPlayers.get(iter).getCurrentStrategy()!=Strategy.STICK)
            {
                allPlayersStickStatus = false;
            }
        }
        return allPlayersStickStatus;
    }
    public void finishGame(String message)
    {
        System.out.println("Game finished. " + message);
        this.gameFinishStatus = true;
    }

    public void goToNextRound()
    {
       this.currentRound +=1;
    }

    public void takeTurn() throws OnePlayerCantPlayException
    {
        if(this.currentPlayers.size() == 1)
        {
            throw new OnePlayerCantPlayException();
        }
        if(this.checkIfAllPlayersStick())
        {
            this.finishGame("All players STICK");
            return;
        }
        for(int iter =0; iter < this.currentPlayers.size(); iter++)
        {
            Players player = this.currentPlayers.get(iter);
            player.playerStatus();
            if(player.getPointsOfCurrentCards() == 21)
            {
                this.finishGame(player.getPlayerName() + " hit exactly " + this.cutOffPoint);
                return;
            }
            else if(player.getPointsOfCurrentCards() < 17)
            {
                this.dealAnotherCard(player);
            }
            else if(player.getPointsOfCurrentCards() > 21)
            {
                this.ejectPlayer(player);
            }
        }
        this.goToNextRound();
    }

    public void determinePlayerWithMaxPointsAtEndOfGame()
    {
        if(this.gameFinishStatus)
        {
            int maxPoints = 0;
            Players topPlayer = new Players("topPlayer");
            for(Players player : this.currentPlayers)
            {
                if(player.getPointsOfCurrentCards() < this.cutOffPoint && player.getPointsOfCurrentCards() > maxPoints)
                {
                    maxPoints = player.getPointsOfCurrentCards();
                    topPlayer = player;
                }
            }
            System.out.println(topPlayer.getPlayerName() + " won with total points of " + topPlayer.getPointsOfCurrentCards());
        }

    }
}
