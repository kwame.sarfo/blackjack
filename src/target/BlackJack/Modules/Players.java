package target.BlackJack.Modules;

import target.BlackJack.Enums.Strategy;

import java.util.ArrayList;
import java.util.List;

public class Players {
    private int points;
    private String playerName;
    private List<Card> currentCards;
    private Strategy currentStrategy;


    private int currentPoints;



    public Players(String playerName)
    {
        this.playerName = playerName;
        this.points = 0;
        this.currentCards = new ArrayList<>();

    }
    public String getPlayerName() {
        return playerName;
    }

    public void receiveCard(Card card)
    {
        this.currentCards.add(card);
    }

    public List<Card> getCurrentCards() {
        return currentCards;
    }

    public int getPointsOfCurrentCards()
    {
        int totalPoints = 0;
        for(Card card : currentCards)
        {
            totalPoints+=card.getCardPoints();
        }
        this.currentPoints = totalPoints;
        ;
        return this.currentPoints;
    }

    public void setCurrentPoints(int currentPoints) {
        this.currentPoints = currentPoints;
        if(this.currentPoints < 17)
        {
            this.currentStrategy = Strategy.HIT;
        }
        else if(this.currentPoints >= 17 && this.currentPoints <=21)
        {
            this.currentStrategy = Strategy.STICK;
        }
        else if(this.currentPoints > 21)
        {
            this.currentStrategy = Strategy.GOBUST;
        }
    }


    public void setCurrentStrategy() {

        if(this.getPointsOfCurrentCards() < 17)
        {
            this.currentStrategy = Strategy.HIT;
        }
        else if(this.getPointsOfCurrentCards() >= 17 && this.currentPoints <=21)
        {
            this.currentStrategy = Strategy.STICK;
        }
        else if(this.getPointsOfCurrentCards() > 21)
        {
            this.currentStrategy = Strategy.GOBUST;
        }
    }

    public Strategy getCurrentStrategy() {
        return this.currentStrategy;
    }



    public void playerStatus()
    {
        System.out.println("");
        System.out.println(this.getPlayerName());
        for (Card card : currentCards)
        {
            System.out.println( card.getCardNumber() + " of " + card.getSuitName() + " " + card.getCardPoints());
        }
        System.out.println("TOTAL POINTS OF " + this.getPlayerName() + " = " + this.getPointsOfCurrentCards());
        System.out.println(this.playerName + " strategy is " + this.getCurrentStrategy());
    }
}
