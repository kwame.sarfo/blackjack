package target.BlackJack.Modules;

public class OnePlayerCantPlayException extends Exception{

    public OnePlayerCantPlayException()
    {
        super("You can't start this game with only One player");
    }
}
