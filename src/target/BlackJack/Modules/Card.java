package target.BlackJack.Modules;

import target.BlackJack.Enums.NumberOnCard;
import target.BlackJack.Enums.Suit;

public class Card {

    private NumberOnCard numberOnCard;
    private Suit suitName;


    public Card(NumberOnCard numberOnCard, Suit suitName)
    {
        this.numberOnCard = numberOnCard;
        this.suitName = suitName;
    }

    public int getCardPoints() {
        return numberOnCard.getPoints();
    }

    public NumberOnCard getCardNumber() {
        return numberOnCard;
    }

    public Suit getSuitName() {
        return suitName;
    }


}
