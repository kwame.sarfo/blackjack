package target.BlackJack.Enums;

public enum Suit {
    HEARTS,
    DIAMOND,
    SPADES,
    CLUBS;
}
